package com.ss8.bsa.service;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.ss8.bsa.config.ConfigProperties;
import com.ss8.bsa.dao.DbDAOOne;
import com.ss8.bsa.exception.ErrorMessage;
import com.ss8.bsa.model.CellTowerSearch;
import com.ss8.bsa.model.CoordinatesRequest;
import com.ss8.bsa.model.GeoPoint;

/**
 * This Class is used to do pre-processing, if required. Method preProcess is
 * called by the CellTowerController for calling the Stored Procedure
 * 
 * @author Srinivas R
 * @version: 1.0
 * 
 */
@Service
public class PreAndPostProcess {

	private static final Logger LOGGER = LoggerFactory.getLogger(PreAndPostProcess.class);

	@Autowired
	private ConfigProperties cfg;

	@Lazy
	@Autowired
	private DbDAOOne dbdaoOne;

	private volatile boolean isCacheBeingRefreshed = false;

	private Map<String, List<CellTowerSearch>> collectCellIdListCGI;
	private Map<String, List<CellTowerSearch>> collectCellIdListECGI;
	private Map<String, List<CellTowerSearch>> collectCellIdListSAI;
	private Map<String, List<CellTowerSearch>> collectCellIdListTAI;
	private Map<String, List<CellTowerSearch>> collectCellIdListRAI;
	private Map<String, List<CellTowerSearch>> collectCellIdListLAI;
	private Map<String, List<CellTowerSearch>> collectCellIdListOthers;

	public PreAndPostProcess() {
		super();
	}

	@PostConstruct
	public void init() {
	}

	public void loadAndRefreshData() {

		String[] supportedcellTypes = cfg.getSupportedCellTypes().split(",");
		for (String supportedCelltype : supportedcellTypes) {

			String celltype = StringUtils.isNotBlank(supportedCelltype)
					? StringUtils.trimToEmpty(supportedCelltype).toUpperCase()
					: null;

				switch (celltype) {
				case (ConfigProperties.CELL_TYPE_CGI):
					collectCellIdListCGI = dbdaoOne.getCellTowerFromCellType(celltype);
					LOGGER.info(celltype + " :: Cache Update/Refresh :: Completed");
					break;
				case (ConfigProperties.CELL_TYPE_ECGI):
					collectCellIdListECGI = dbdaoOne.getCellTowerFromCellType(celltype);
					LOGGER.info(celltype + " :: Cache Update/Refresh :: Completed");
					break;
				case (ConfigProperties.CELL_TYPE_SAI):
					collectCellIdListSAI = dbdaoOne.getCellTowerFromCellType(celltype);
					LOGGER.info(celltype + " :: Cache Update/Refresh :: Completed");
					break;
				case (ConfigProperties.CELL_TYPE_TAI):
					collectCellIdListTAI = dbdaoOne.getCellTowerFromCellType(celltype);
					LOGGER.info(celltype + " :: Cache Update/Refresh :: Completed");
					break;
				case (ConfigProperties.CELL_TYPE_RAI):
					collectCellIdListRAI = dbdaoOne.getCellTowerFromCellType(celltype);
					LOGGER.info(celltype + " :: Cache Update/Refresh :: Completed");
					break;
				case (ConfigProperties.CELL_TYPE_LAI):
					collectCellIdListRAI = dbdaoOne.getCellTowerFromCellType(celltype);
					LOGGER.info(celltype + " :: Cache Update/Refresh :: Completed");
					break;
				default:
					collectCellIdListOthers = dbdaoOne.getCellTowerFromCellTypeOthers();
					LOGGER.info("Other CellTypes :: Cache Update/Refresh :: Completed");
				}
		}

		LOGGER.info("Others :: Cache Update/Refresh :: Completed");

	}

	@Scheduled(fixedDelayString = "${bsa.cache_refresh_intervel_seconds:3600}000", initialDelay = 10)
	// @Scheduled(fixedDelayString = "${bsa.cache_refresh_cron}")
	public void cacheScheduler() {
		isCacheBeingRefreshed = true;
		LOGGER.info("BSAlmanac Service :: Cache Update/Refresh :: Started");
		loadAndRefreshData();
		LOGGER.info("BSAlmanac Service :: Cache Update/Refresh :: Completed");
		isCacheBeingRefreshed = false;
	}

	/**
	 * This method does the following: Accepts a json array string and calls the
	 * Stored Procedure
	 * 
	 * @param String jsonArray
	 * @return String
	 */
	public String preProcess(String cellJsonArray) {
		try {
			if (StringUtils.isBlank(cellJsonArray)) {
				return new ErrorMessage(422, "Empty Request").toString();
			} else {
				return dbdaoOne.callGeometrySP(cellJsonArray).toString();
			}
		} catch (Exception e) {
			return new ErrorMessage(422, e.getMessage()).toString();
		}
	}

	/**
	 * This method does the following: Accepts a json array string, seraches the BSA
	 * data and returns List of co-ordinates
	 * 
	 * @param List<CoordinatesRequest>
	 * @return List<GeoPoint>
	 */

	public List<GeoPoint> preProcessLatLongCoordinates(List<CoordinatesRequest> coordinatesRequests) {

		List<GeoPoint> eventLocation = new ArrayList<GeoPoint>();

		for (CoordinatesRequest cRequest : coordinatesRequests) {

			GeoPoint geoPoint = new GeoPoint();

			if (StringUtils.isNotBlank(cRequest.getEventId()))
				geoPoint.setEventId(StringUtils.trimToEmpty(cRequest.getEventId()));

			LocalDateTime eventDateLDT = cRequest.getEventdate() != null
					? getDateFormats(StringUtils.trimToEmpty(cRequest.getEventdate()))
					: null;

			if (eventDateLDT == null) {
				eventLocation.add(geoPoint);
				continue;
			}

			String cellId = StringUtils.isNotBlank(cRequest.getCellId()) ? StringUtils.trimToEmpty(cRequest.getCellId())
					: null;

			if (StringUtils.isBlank(cellId)) {
				eventLocation.add(geoPoint);
				continue;
			}

			String cellType = StringUtils.isNotBlank(cRequest.getCellType())
					? StringUtils.trimToEmpty(cRequest.getCellType()).toUpperCase()
					: null;

			if (isCacheBeingRefreshed) {
				LOGGER.info(LocalDateTime.now() + " :: BSAlmanac Service :: Cache Update/Refresh :: In Progress");
				geoPoint = searchCoordinatesFromDB(cellId, eventDateLDT, geoPoint);
			} else {
				if (StringUtils.isNotBlank(cellType)) {
					if (cellType.equalsIgnoreCase(ConfigProperties.CELL_TYPE_CGI)) {
						geoPoint = searchCoordinates(collectCellIdListCGI, cellId, eventDateLDT, geoPoint);
					} else if (cellType.equalsIgnoreCase(ConfigProperties.CELL_TYPE_ECGI)) {
						geoPoint = searchCoordinates(collectCellIdListECGI, cellId, eventDateLDT, geoPoint);
					} else if (cellType.equalsIgnoreCase(ConfigProperties.CELL_TYPE_SAI)) {
						geoPoint = searchCoordinates(collectCellIdListSAI, cellId, eventDateLDT, geoPoint);
					} else if (cellType.equalsIgnoreCase(ConfigProperties.CELL_TYPE_TAI)) {
						geoPoint = searchCoordinates(collectCellIdListTAI, cellId, eventDateLDT, geoPoint);
					} else if (cellType.equalsIgnoreCase(ConfigProperties.CELL_TYPE_RAI)) {
						geoPoint = searchCoordinates(collectCellIdListRAI, cellId, eventDateLDT, geoPoint);
					} else if (cellType.equalsIgnoreCase(ConfigProperties.CELL_TYPE_LAI)) {
						geoPoint = searchCoordinates(collectCellIdListLAI, cellId, eventDateLDT, geoPoint);
					} else {
						geoPoint = searchCoordinates(collectCellIdListOthers, cellId, eventDateLDT, geoPoint);
					}
				} else {
					geoPoint = searchCoordinatesWithoutCellType(cellId, eventDateLDT, geoPoint);
				}
			}

			eventLocation.add(geoPoint);
		}

		return eventLocation;

	}

	private GeoPoint searchCoordinates(Map<String, List<CellTowerSearch>> collectCellIdList, String cellid,
			LocalDateTime eventDate, GeoPoint geopoint) {
		if (!CollectionUtils.isEmpty(collectCellIdList)) {
			if (collectCellIdList.containsKey(cellid)) {
				List<CellTowerSearch> listForCellIdMatch = collectCellIdList.get(cellid);
				for (CellTowerSearch celltower : listForCellIdMatch) {
					LocalDateTime validFrom = celltower.getValidFrom();
					LocalDateTime validTo = celltower.getValidTo();
					boolean isAfterOrEqual = eventDate.isAfter(validFrom) || eventDate.isEqual(validFrom);
					boolean isBeforeOrEqual = eventDate.isBefore(validTo) || eventDate.isEqual(validTo);
					if (isAfterOrEqual && isBeforeOrEqual) {
						geopoint.setCoordinates(celltower.getLatlong());
						geopoint.setAddress(celltower.getAddress());
						return geopoint;
					}
				}
			} 
				
		} 
		
		geopoint.setCoordinates(new double[0]);
		geopoint.setAddress(StringUtils.EMPTY);
		return geopoint;
		
	}

	private GeoPoint searchCoordinatesFromDB(String cellid, LocalDateTime eventDate, GeoPoint geopoint) {
		Map<String, List<CellTowerSearch>> cellTowerSearchAllFromDB = dbdaoOne.getCellTowerFromCellIdSearch(cellid);
		if (!CollectionUtils.isEmpty(cellTowerSearchAllFromDB))
			geopoint = searchCoordinates(cellTowerSearchAllFromDB, cellid, eventDate, geopoint);
		return geopoint;
	}

	private GeoPoint searchCoordinatesWithoutCellType(String cellid, LocalDateTime eventDate, GeoPoint geopoint) {
		// If more than one identity of different type is present,
		// Then they shall be sorted in the following order: CGI, SAI, RAI, TAI, ECGI, LAI.

		geopoint = searchCoordinates(collectCellIdListCGI, cellid, eventDate, geopoint);
		if (geopoint.getCoordinates().length != 0)
			return geopoint;
		else {
			geopoint = searchCoordinates(collectCellIdListSAI, cellid, eventDate, geopoint);
			if (geopoint.getCoordinates().length != 0)
				return geopoint;
			else {
				geopoint = searchCoordinates(collectCellIdListRAI, cellid, eventDate, geopoint);
				if (geopoint.getCoordinates().length != 0)
					return geopoint;
				else {
					geopoint = searchCoordinates(collectCellIdListTAI, cellid, eventDate, geopoint);
					if (geopoint.getCoordinates().length != 0)
						return geopoint;
					else {
						geopoint = searchCoordinates(collectCellIdListECGI, cellid, eventDate, geopoint);
						if (geopoint.getCoordinates().length != 0)
							return geopoint;
						else {
							geopoint = searchCoordinates(collectCellIdListLAI, cellid, eventDate, geopoint);
							if (geopoint.getCoordinates().length != 0)
								return geopoint;
							else {
								geopoint = searchCoordinates(collectCellIdListOthers, cellid, eventDate, geopoint);
								if (geopoint.getCoordinates().length != 0)
									return geopoint;
								else {
									geopoint.setCoordinates(new double[0]);
									return geopoint;
								}
							}
						}
					}
				}
			}
		}
	}

	public LocalDateTime getDateFormats(String dateString) {
		try {
			OffsetDateTime odt = OffsetDateTime.parse(dateString);
			LocalDateTime dateTimeWithoutOffset = odt.withOffsetSameInstant(ZoneOffset.UTC).toLocalDateTime();
			return dateTimeWithoutOffset;
		} catch (Exception e) {
			LOGGER.error(LocalDateTime.now() + " :: " + dateString + " :: This Date Format is Not Supported!..");
			return null;
		}
	}

}
