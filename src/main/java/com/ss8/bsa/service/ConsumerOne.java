package com.ss8.bsa.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ss8.bsa.config.ConfigProperties;
import com.ss8.bsa.dao.DbDAOOne;
import com.ss8.bsa.model.CellTower;

@Service
public class ConsumerOne implements Runnable {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerOne.class);

	private final static ObjectMapper objectMapper = new ObjectMapper();

	@Autowired
	private ConfigProperties cfg;

	private static int consumerLastDelay;
	private static int dbBatchSize;

	@Autowired
	@Lazy
	private DbDAOOne dbdaoOne;

	private BlockingQueue<String> queueOne;

	public ConsumerOne() {
	}

	@PostConstruct
	public void init() {
		consumerLastDelay = cfg.getConsumerlastDelayInMs();
		dbBatchSize = cfg.getDbBatchsize();
	}

	public BlockingQueue<String> getQueueOne() {
		return queueOne;
	}

	public void setQueueOne(BlockingQueue<String> qOne) {
		this.queueOne = qOne;
	}

	@Override
	public void run() {
		try {
			String data;
			// consuming messages until done message is received
			while ((data = queueOne.take()) != "done") {
				parseJsonStringSendToDB(data);
				Thread.sleep(consumerLastDelay);
			}
		} catch (Exception e) {
			LOGGER.error(LocalDateTime.now() + " Exception :: " + e.getMessage());
		}
	}

	/**
	 * This method does the following: 1. Receives Json Filename, Parses the file,
	 * Validates the Cell Id and calls the dB insertion method of the DbDAOOne
	 * 
	 * @param String fullFilename
	 * 
	 */
	public void parseJsonStringSendToDB(String jsonArrayString) {

		List<CellTower> celltowerRows = null;
				
		try {
			celltowerRows = objectMapper.readValue(jsonArrayString, new TypeReference<List<CellTower>>() {
			});
		} catch (Exception e1) {
			LOGGER.error("Exception :: {}", e1.getMessage());
		}

		int recordsSize = celltowerRows.size();
		int i = 0;
		int j = 0;

		if (!CollectionUtils.isEmpty(celltowerRows)) {
			
			LOGGER.info("--------------------------------------------------------------------------");
			LOGGER.info(LocalDateTime.now() + " :: Proceeding to DB Insertion");

			for (int cnt=0; cnt<celltowerRows.size(); cnt++) {
		
				if (recordsSize < dbBatchSize) {
					if (++j == recordsSize) {
						try {
							dbdaoOne.insertCellTowerList(celltowerRows);
						} catch (Exception e) {
							LOGGER.error(LocalDateTime.now() + "Exception :: {}", e.getMessage());
						} finally {
							celltowerRows.clear();
						}
					}
				} else {
					if (++i % dbBatchSize == 0) {
						try {
							dbdaoOne.insertCellTowerList(celltowerRows);
						} catch (Exception e) {
							LOGGER.error(LocalDateTime.now() + "Exception :: {}", e.getMessage());
						} finally {
							celltowerRows.clear();
						}
					} else {
						if (i == recordsSize) {
							try {
								dbdaoOne.insertCellTowerList(celltowerRows);
							} catch (Exception e) {
								LOGGER.error(LocalDateTime.now() + "Exception :: {}", e.getMessage());
							} finally {
								celltowerRows.clear();
							}
						}
					}
				}
			}
		}
	}

}
