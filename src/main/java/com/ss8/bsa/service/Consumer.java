package com.ss8.bsa.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.ss8.bsa.config.ConfigProperties;

@Component
public class Consumer implements Runnable {

	private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);

	private BlockingQueue<String> queue;
	private BlockingQueue<String> queueOne;
	private String geoJsonUrl;
	private List<MediaType> acceptableMediaTypes;
	private HttpHeaders headers;
	URI uri;
	
	private static int producerTwoDelay;

	@Autowired
	private ConfigProperties cfg;

	@Lazy
	@Autowired
	private RestTemplate restTemplate;

	@Bean
	private RestTemplate restTemplate() {
		return new RestTemplate();
	}

	public Consumer() {
	}

	public BlockingQueue<String> getQueue() {
		return queue;
	}

	public void setQueue(BlockingQueue<String> queue) {
		this.queue = queue;
	}

	public BlockingQueue<String> getQueueOne() {
		return queueOne;
	}

	public void setQueueOne(BlockingQueue<String> queueOne) {
		this.queueOne = queueOne;
	}

	@PostConstruct
	private void init() {
		geoJsonUrl = cfg.getGeojsonRestUrl();
		producerTwoDelay = cfg.getProducertwoDelayInMs();
		if (restTemplate == null) {
			restTemplate = restTemplate();
		}
		initiHttpPost();
	}
	
	private void initiHttpPost() {
		acceptableMediaTypes = new ArrayList<MediaType>();
		headers = new HttpHeaders();
		headers.setAccept(acceptableMediaTypes);
		headers.setContentType(MediaType.APPLICATION_JSON);
		try {
			uri = new URI(geoJsonUrl);
		} catch (URISyntaxException e) {
			LOGGER.error(LocalDateTime.now() + " :: Exception :: " + e.getMessage());
		}
	
	}
	

	@Override
	public void run() {
		try {
			String data;
			// consuming messages until done message is received
			while ((data = queue.take()) != "done") {
				String postResponse = this.sendPostRequestToPythonScript(data);
				//System.out.println(postResponse);
				if (postResponse != null)
					queueOne.put(postResponse);
				Thread.sleep(producerTwoDelay);
			}
		} catch (InterruptedException e) {
			LOGGER.error(LocalDateTime.now() + " Exception :: " + e.getMessage());
		}
	}

	public String sendPostRequestToPythonScript(String jsonArrayString) {
		try {
			HttpEntity<String> entity = new HttpEntity<String>(jsonArrayString, headers);
			ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
			return (response.getBody());
		} catch (Exception e) {
			LOGGER.error(LocalDateTime.now() + " :: Exception :: " + e.getMessage());
			return null;
		}
	}

}
