package com.ss8.bsa.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.ss8.bsa.config.ConfigProperties;

/**
 * This Class Does the Following: Executes the Python script periodically,
 * Parses all the JSON files generated by the Python script, Processes them and
 * sends them into the db by calling the DbDAOOne object's insertCellTowerList
 * method. Consists of the following methods: Csv2DBMain, parseCSVFileSendToDB,
 * backupFiles & runPythonScript
 * 
 * @author Srinivas R
 * @version: 1.0 
 */

@Component
public class Csv2DB {

	private static final Logger LOGGER = LoggerFactory.getLogger(Csv2DB.class);

	@Autowired
	private ConfigProperties cfg;

	private static String csvSourceFolderPath;
	private static String csvBackupFolderPath;
	private static int csvToJsonArraySize;
	private static int producerOneDelay;

	private BlockingQueue<String> queue;
	private BlockingQueue<String> queueOne;

	@Autowired
	private Consumer consumer;

	@Autowired
	private ConsumerOne consumerOne;

	@PostConstruct
	public void init() {
		csvSourceFolderPath = cfg.getCsvfilesSourceFolder();
		csvBackupFolderPath = cfg.getCsvfilesBackupFolder();
		csvToJsonArraySize = cfg.getCsvtojsonArraysize();
		producerOneDelay = cfg.getProduceroneDelayInMs();
		this.queue = new LinkedBlockingQueue<>();
		this.queueOne = new LinkedBlockingQueue<>();
		consumer.setQueue(queue);
		consumer.setQueueOne(queueOne);
		consumerOne.setQueueOne(queueOne);
		new Thread(consumer).start();
		new Thread(consumerOne).start();
	}

	/**
	 * This method does the following: 1. Retrieves the list of all Json files in a
	 * folder 2. Parse each file and maps the rows into the CellTower objects, fills
	 * an ArrayList of these objects, sends them to the db insert and backs up the
	 * files This method is executed at a fixed interval(configurable in
	 * application.properties file)
	 * 
	 */
	@Scheduled(fixedDelayString = "${bsa.csvfolder_scan_intervel_seconds:3600}000", initialDelay = 10000)
	public void csvToDBMain() {

		Path folderPath = Paths.get(csvSourceFolderPath);
		List<String> fileNames = new ArrayList<>();

		/**
		 * Retrieve a list of the files from the source folder
		 * 
		 * @throws IOException
		 */
		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(folderPath)) {
			for (Path path : directoryStream) {
				fileNames.add(path.toString());
			}
		} catch (IOException ex) {
			LOGGER.error(LocalDateTime.now() + " :: Exception :: " + ex.getMessage());
		}

		/**
		 * Go through the list of files, Convert the csv content to Json Byte Array,
		 * Send this Json Array as Request to the Python Rest Back up the files
		 */
		if (fileNames.size() != 0) {

			boolean parseCSVResult = false;
			boolean backupFiles = false;

			for (String file : fileNames) {
				try {
					parseCSVResult = parseCSVFileToJSONArray(file);
					backupFiles = backupFiles(file);
				} catch (Exception e) {
					LOGGER.error(LocalDateTime.now() + " :: Exception :: " + e.getMessage());
				}
			}

			if (parseCSVResult == true && backupFiles == true) {
				LOGGER.info(LocalDateTime.now() + " :: Parsed and Backed up the CSV files Succesfully");
			}
		}

	}
	
	
	
	/**
	 * This method does the following: 1. Receives CSV Filename, Parses the file,
	 * Converts it into a JSON Array of 1000 records each
	 * 
	 * @param String fullFilename
	 * 
	 */
	public boolean parseCSVFileToJSONArray(String fullFilename) throws IOException {

		CsvMapper csvMapper = new CsvMapper();
		CsvSchema schema = CsvSchema.emptySchema().withHeader();

		ObjectReader objectReader = csvMapper.readerFor(Map.class).with(schema);
		MappingIterator<Map<String, String>> it = null;
		ObjectMapper mapper = new ObjectMapper();

		try (Reader reader = new FileReader(fullFilename)) {
			int i = 0;
			List<Object> jList = new ArrayList<>();
			List<Object> jRemainingList = new ArrayList<>();

			it = objectReader.readValues(reader);
		
			while (it.hasNext()) {
				
				Map<String, String> next = it.next();
				
				if(next.get("width")==null) {
					next.put("width", "0");
				} else if ((StringUtils.isEmpty(next.get("width").trim()))) {
					next.put("width", "0");
				}

				if(next.get("azimuth")==null) {
					next.put("azimuth", "0");
				} else if ((StringUtils.isEmpty(next.get("azimuth").trim()))) {
					next.put("azimuth", "0");
				}
				
				if(next.get("range")==null) {
					next.put("range", "0");
				} else if ((StringUtils.isEmpty(next.get("range").trim()))) {
					next.put("range", "0");
				}
				
				if(next.get("lon")==null) {
					next.put("lon", "0");
				} else if ((StringUtils.isEmpty(next.get("lon").trim()))) {
					next.put("lon", "0");
				}
				
				if(next.get("lat")==null) {
					next.put("lat", "0");
				} else if ((StringUtils.isEmpty(next.get("lat").trim()))) {
					next.put("lat", "0");
				}
				
				if(next.get("valid_from")==null) {
					next.put("valid_from", Instant.now().toString());
				} else if ((StringUtils.isEmpty(next.get("valid_from").trim()))) {
					next.put("valid_from", Instant.now().toString());
				}
				
				if(next.get("valid_to")==null) {
					next.put("valid_to", ZonedDateTime.now(ZoneOffset.UTC).plusYears(3).toInstant().toString());
				} else if ((StringUtils.isEmpty(next.get("valid_to").trim()))) {
					next.put("valid_to", ZonedDateTime.now(ZoneOffset.UTC).plusYears(3).toInstant().toString());
				}
				
				jList.add(next);
				++i;
				if (i % csvToJsonArraySize == 0) {
					jRemainingList.clear();
					String jsonArrayStringFromCsv = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jList);
					try {
						queue.put(jsonArrayStringFromCsv);
						Thread.sleep(producerOneDelay);
					} catch (InterruptedException e) {
						LOGGER.error(LocalDateTime.now() + " Exception :: " + e.getMessage());
					}
					jList.clear();
				} else {
					jRemainingList.add(next);
				}

			}

			String jsonArrayStringRemainingFromCsv = mapper.writerWithDefaultPrettyPrinter()
					.writeValueAsString(jRemainingList);
			try {
				queue.put(jsonArrayStringRemainingFromCsv);
				Thread.sleep(producerOneDelay);
			} catch (InterruptedException e) {
				LOGGER.error(LocalDateTime.now() + " Exception :: " + e.getMessage());
			}

		} catch (FileNotFoundException e) {
			LOGGER.error(LocalDateTime.now() + " Exception :: " + e.getMessage());
			return false;
		} catch (JsonProcessingException e) {
			LOGGER.error(LocalDateTime.now() + " Exception :: " + e.getMessage());
			return false;
		} catch (IOException e) {
			LOGGER.error(LocalDateTime.now() + " Exception :: " + e.getMessage());
			return false;
		} catch (Exception e) {
			LOGGER.error(LocalDateTime.now() + " Exception :: " + e.getMessage());
			return false;
		}

		return true;

	}

	/**
	 * This method does the following: Takes a file, renames it into .bak and moves
	 * it into a backup folder. The source and backup folders can be configured in
	 * the application.properties file
	 * 
	 * @param fullFilename Filename to be backed up
	 * 
	 */
	public boolean backupFiles(String fullFilename) {
		
		try {
			File file = new File(fullFilename);
			String filenameInString = file.getName();
			String backupFilename = csvBackupFolderPath.concat(filenameInString).concat(".bak");
			/**
			 * Move the file to a backup folder
			 * 
			 * @throws IOException
			 */
			Path tempPath = Files.move(Paths.get(fullFilename), Paths.get(backupFilename),
					StandardCopyOption.REPLACE_EXISTING);
			if (tempPath != null) {
				LOGGER.info(LocalDateTime.now() + " :: File: " + fullFilename + " moved into the backup folder as :: " + backupFilename);
				return true;
			} else {
				LOGGER.error(LocalDateTime.now() + " :: Failed to move the file to the backup folder");
				return false;
			}
		} catch (IOException e) {
			LOGGER.error(LocalDateTime.now() + " :: Exception :: " + e.getMessage());
			return false;
		}

	}

}
