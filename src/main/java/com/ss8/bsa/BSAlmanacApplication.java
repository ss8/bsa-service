package com.ss8.bsa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

/**
* Spring Starter Class
* This is the Entry point to the application.
**/

@EnableScheduling
@EnableEncryptableProperties
@SpringBootApplication
public class BSAlmanacApplication {

	public static void main(String[] args) {
		SpringApplication.run(BSAlmanacApplication.class, args);
	}
	
	

}
