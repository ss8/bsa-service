package com.ss8.bsa.controller;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ss8.bsa.config.ConfigProperties;
import com.ss8.bsa.exception.ErrorMessage;
import com.ss8.bsa.model.CoordinatesRequest;
import com.ss8.bsa.model.GeoPoint;
import com.ss8.bsa.service.PreAndPostProcess;

/**
 * REST Endpoint Controller Class Contains a method getFeature which exposes the
 * interface /getfeature. Takes a Json Array as input, returns a Json String.
 * @author Srinivas R
 * @version: 1.0 
 */
@RestController
@RequestMapping(value = "/")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CellTowerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CellTowerController.class);
	
	@Autowired
	private PreAndPostProcess pProcess;

	private final static ObjectMapper objectMapper = new ObjectMapper();

	@PostConstruct
	public void init() {
	}

	/**
	 * Method getFeature which exposes the interface /getfeature. Takes a Json Array
	 * as input, returns a Json String.
	 * 
	 * @param cellJsonArray
	 * @return JsonString
	 */
	@PostMapping(value = ConfigProperties.REST_API_GET_FEATURE, produces = "application/json")
	public String getFeature(@RequestBody(required = true) String cellJsonArray) {
		LOGGER.debug("Rest Interface Received Request :: {} ", cellJsonArray);
		return pProcess.preProcess(cellJsonArray).toString();
	}
	
	
	/**
	 * Method getLatLongFeature exposes the interface /getlatlongfeature. Takes a
	 * Json Array as input, returns a Json String.
	 * 
	 * @param inputFeatures Json Array
	 * @return JsonString
	 */
	@PostMapping(value = ConfigProperties.REST_API_GET_LATLONG_FEATURE, produces = "application/json")
	public ResponseEntity<String> getLatLongFeature(@RequestBody(required = true) List<CoordinatesRequest> inputFeatures) {
		HttpHeaders headers = new HttpHeaders();
		LOGGER.debug("Rest Interface Received Request :: {} ", inputFeatures);
		try {
			List<GeoPoint> response = pProcess.preProcessLatLongCoordinates(inputFeatures);
			return new ResponseEntity<String>(
					objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(response), headers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(new ErrorMessage(422, "Empty or Null Response").toString(), headers,
					HttpStatus.valueOf(422));
		} 
		
	}
	

}
