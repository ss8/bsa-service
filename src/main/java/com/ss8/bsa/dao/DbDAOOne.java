package com.ss8.bsa.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.GenericStoredProcedure;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.ss8.bsa.config.ConfigProperties;
import com.ss8.bsa.exception.ErrorMessage;
import com.ss8.bsa.model.CellTower;
import com.ss8.bsa.model.CellTowerFeature;
import com.ss8.bsa.model.CellTowerSearch;

/**
 * DbDAO Class. Used for JDBC Insert and Calling the Stored Procedure. Contains
 * the methods: init, createJdbcTemplateDbOne, insertCellTowerList and
 * callGeometrySP.
 * 
 * @author Srinivas R
 * @version: 1.0
 */
@Component
public class DbDAOOne {

	private static final Logger LOGGER = LoggerFactory.getLogger(DbDAOOne.class);

	private static final String SP_NAME = "getcelltowerfeaturetz";
	private static final String INSERT_QUERY = "INSERT INTO celltower(id, type, cellid, valid_from, valid_to, timezone, metadata, address, geometry, latlong) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" + " ON DUPLICATE KEY UPDATE type=VALUES(type), cellid=VALUES(cellid), valid_from=VALUES(valid_from), valid_to=VALUES(valid_to), timezone=VALUES(timezone), metadata = VALUES(metadata), address = VALUES(address), geometry = VALUES(geometry), latlong = VALUES(latlong)";

	
	@Qualifier("jdbcdatasourceone")
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	@Qualifier("datasourceone")
	DataSource dataSourceOne;
	
	private StoredProcedure procedure;

	public DbDAOOne() {
	}

	@PostConstruct
	public void init() {
		setSPConfig();
	}

	public void setSPConfig() {
		procedure = new GenericStoredProcedure();
		procedure.setDataSource(dataSourceOne);
		procedure.setSql(SP_NAME);
		procedure.setFunction(false);
		SqlParameter[] parameters = { new SqlParameter(Types.VARCHAR) };
		procedure.setParameters(parameters);
		procedure.compile();
	}

	/**
	 * This method accepts a List of CellTower Objects and Batch inserts them into
	 * the dB using JdbcTemplate's batchUpdate method
	 * 
	 * @param List<CellTower> celltowerList
	 */
	public void insertCellTowerList(List<CellTower> celltowerList) {

		Instant start = Instant.now();

		try {
			
			LOGGER.info(LocalDateTime.now() + " :: DB Insert/Update - Started");

			jdbcTemplate.batchUpdate(INSERT_QUERY, new BatchPreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					CellTower celltowerSingleRecord = celltowerList.get(i);
					ps.setString(1, "0");
					ps.setString(2, celltowerSingleRecord.getType());
					ps.setString(3, celltowerSingleRecord.getCellid());
					ps.setTimestamp(4, celltowerSingleRecord.getValidFromTimestamp());
					ps.setTimestamp(5, celltowerSingleRecord.getValidToTimestamp());
					ps.setString(6, celltowerSingleRecord.getOffset());
					ps.setString(7, celltowerSingleRecord.getMetadata());
					ps.setString(8, celltowerSingleRecord.getAddress());
					ps.setString(9, celltowerSingleRecord.getGeometry().toString());
					ps.setString(10, celltowerSingleRecord.getLatLong());
				}

				@Override
				public int getBatchSize() {
					return celltowerList.size();
				}
			});

			LOGGER.info(LocalDateTime.now() + " :: Thread :: " + Thread.currentThread().getId()
					+ " :: Inserted/Updated Batch Size: " + celltowerList.size());
			LOGGER.info("--------------------------------------------------------------------------");

		} catch (Exception e) {
			LOGGER.error(LocalDateTime.now() + " :: DB Insertion Failed :: Batch Size :: " + celltowerList.size()
					+ "\nException :: " + e.getMessage());
			LOGGER.error("First Record :: ");
			LOGGER.error(celltowerList.get(0).toString());
			LOGGER.error("Last Record :: ");
			LOGGER.error(celltowerList.get(celltowerList.size() - 1).toString());
		} finally {
			celltowerList.clear();
			Instant stop = Instant.now();
			long timeElapsed = Duration.between(start, stop).toMillis();
			LOGGER.info(LocalDateTime.now() + " Time Taken :: " + timeElapsed + " Milliseconds");
			LOGGER.info("--------------------------------------------------------------------------");
		}

	}

	/**
	 * This method is used to call the Stored Procedure. Accepts a Json String and
	 * returns a Json String
	 * 
	 * @param String inGeometry
	 * @return String result
	 */
	public Object callGeometrySP(String inGeometry) {

		Map<String, Object> result = null;
		try {
			result = procedure.execute(inGeometry);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			if (e.getMessage().contains("Access denied for user")) {
				return new ErrorMessage(500, "Invalid dB Settings").toString();
			} else if (e.getMessage().contains("No timezone mapping entry")) {
				return new ErrorMessage(500, "Invalid dB Settings").toString();
			} else if (e.getMessage().contains("bad SQL grammar")) {
				return new ErrorMessage(500, "Invalid dB Settings or Bad Request").toString();
			} else if (e.getMessage().contains("400") && e.getMessage().contains("Bad Request")) {
				return new ErrorMessage(400, "HTTP 400 - Bad Request").toString();
			}
		}

		ArrayList<?> arrayList = (ArrayList<?>) result.get("#result-set-1");
		ArrayList<String> resultList = new ArrayList<String>();

		for (int i = 0; i < arrayList.size(); i++) {
			Map<?, ?> resultMap = (Map<?, ?>) arrayList.get(i);
			resultList.add((String) resultMap.get("geometry"));
		}

		return new CellTowerFeature(resultList);

	}

	public Map<String, List<CellTowerSearch>> getCellTowerFromCellType(String celltype) {
		String searchQueryCellType = "SELECT cellid, valid_from, valid_to, latlong, address FROM celltower WHERE type='" + celltype + "'";
		List<CellTowerSearch> cellIdList;
		try {
			cellIdList = jdbcTemplate.query(searchQueryCellType, new BeanPropertyRowMapper<CellTowerSearch>(CellTowerSearch.class));
			if (CollectionUtils.isEmpty(cellIdList))
				return null;
			else
				return cellIdList.stream().collect(Collectors.groupingBy(CellTowerSearch::getCellid,
						Collectors.mapping(CellTowerSearch -> CellTowerSearch, Collectors.toList())));
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	
	public Map<String, List<CellTowerSearch>> getCellTowerFromCellTypeOthers() {
		String searchQueryCellType = "SELECT cellid, valid_from, valid_to, latlong, address FROM celltower WHERE type!= '" + ConfigProperties.CELL_TYPE_CGI + "' AND type!= '" + ConfigProperties.CELL_TYPE_ECGI + "' AND type!= '" + ConfigProperties.CELL_TYPE_RAI + "' AND type!= '" + ConfigProperties.CELL_TYPE_SAI + "' AND type!= '" + ConfigProperties.CELL_TYPE_TAI + "' AND type!= '" + ConfigProperties.CELL_TYPE_LAI + "' ";
		List<CellTowerSearch> cellIdList;
		try {
			cellIdList = jdbcTemplate.query(searchQueryCellType, new BeanPropertyRowMapper<CellTowerSearch>(CellTowerSearch.class));
			if (CollectionUtils.isEmpty(cellIdList))
				return null;
			else
				return cellIdList.stream().collect(Collectors.groupingBy(CellTowerSearch::getCellid,
						Collectors.mapping(CellTowerSearch -> CellTowerSearch, Collectors.toList())));
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}



	public Map<String, List<CellTowerSearch>> getCellTowerFromCellIdSearch(String cellId) {
		String searchQueryCellId = "SELECT cellid, valid_from, valid_to, latlong, address FROM celltower WHERE cellid='" + cellId + "'";
		try {
			List<CellTowerSearch> cellIdList = jdbcTemplate.query(searchQueryCellId, new BeanPropertyRowMapper<CellTowerSearch>(CellTowerSearch.class));
			if (CollectionUtils.isEmpty(cellIdList))
				return null;
			else
			return cellIdList.stream()
					.collect(Collectors.groupingBy(CellTowerSearch::getCellid,
							Collectors.mapping(CellTowerSearch -> CellTowerSearch, Collectors.toList())));
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}

	
	}

}
