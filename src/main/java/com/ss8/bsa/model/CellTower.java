package com.ss8.bsa.model;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * CellTower Class is the bean used for mapping the CSV/JSON parameters
 * 
 * @author Srinivas R
 * @version: 1.0
 */
@Repository
@JsonIgnoreProperties(ignoreUnknown = true)
public class CellTower {

	private static final Logger LOGGER = LoggerFactory.getLogger(CellTower.class);

	private String type;
	private BigInteger id;
	private String cellid;
	private double lon;
	private double lat;
	@JsonProperty("valid_from")
	private String validFrom;
	@JsonProperty("valid_to")
	private String validTo;
	private String metadata;
	private String address;
	private JsonNode geometry;

	public CellTower() {
		super();
	}

	public CellTower(String type, String cellid, double lon, double lat, String validFrom, String validTo,
			String metadata, String address, JsonNode geometry) {
		super();
		this.type = type;
		this.cellid = cellid;
		this.lon = lon;
		this.lat = lat;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.metadata = metadata;
		this.address = address;
		this.geometry = geometry;

	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCellid() {
		return cellid;
	}

	public void setCellid(String cellid) {
		this.cellid = cellid;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	public String getValidTo() {
		return validTo;
	}

	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public JsonNode getGeometry() {
		return geometry;
	}

	public void setGeometry(JsonNode geometry) {
		this.geometry = geometry;
	}

	public String getLatLong() {
		return Arrays.toString(new double[] { this.getLat(), this.getLon() });
	}

	/**
	 * This method accepts different formats of datetime as String and returns the
	 * Timestamp.
	 * 
	 * @return Timestamp
	 */

	public Timestamp getValidFromTimestamp() {
		try {
			OffsetDateTime odt = OffsetDateTime.parse(this.getValidFrom());
			LocalDateTime dateTimeWithoutOffset = odt.withOffsetSameInstant(ZoneOffset.UTC).toLocalDateTime();
			return Timestamp.valueOf(dateTimeWithoutOffset);
		} catch (Exception e) {
			LOGGER.error(
					LocalDateTime.now() + " :: " + this.getValidFrom() + " :: This Date Format is Not Supported!..");
			return null;
		}
	}

	/**
	 * This method accepts different formats of datetime as String and returns the
	 * Timestamp.
	 * 
	 * @return Timestamp
	 */

	public Timestamp getValidToTimestamp() {
		try {
			OffsetDateTime odt = OffsetDateTime.parse(this.getValidTo());
			LocalDateTime dateTimeWithoutOffset = odt.withOffsetSameInstant(ZoneOffset.UTC).toLocalDateTime();
			return Timestamp.valueOf(dateTimeWithoutOffset);
		} catch (Exception e) {
			LOGGER.error(LocalDateTime.now() + " :: " + this.getValidTo() + " :: This Date Format is Not Supported!..");
			return null;
		}
	}

	/**
	 * This method accepts different formats of datetime as String and returns the
	 * Timestamp.
	 * 
	 * @param String dateString
	 * @return String[]
	 */

	public String getOffset() {
		try {
			OffsetDateTime odt = OffsetDateTime.parse(this.validTo);
			String offsetString = odt.getOffset().toString();
			if (offsetString.equals("Z"))
				offsetString = "+0:00";
			return offsetString;
		} catch (Exception e) {
			LOGGER.error(LocalDateTime.now() + " :: " + this.validTo + " :: This Date Format is Not Supported!..");
			return null;
		}
	}

	@Override
	public String toString() {
		return "CellTower [type=" + type + ", id=" + id + ", cellid=" + cellid + ", lon=" + lon + ", lat=" + lat
				+ ", validFrom=" + validFrom + ", validTo=" + validTo + ", metadata=" + metadata + ", address="
				+ address + ", geometry=" + geometry + "]";
	}

}
