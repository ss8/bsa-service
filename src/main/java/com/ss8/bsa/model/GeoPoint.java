package com.ss8.bsa.model;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoPoint {
	
	@JsonProperty("event_id")
	private String eventId;
	
	private double[] coordinates;
	
	private String address;
	
	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public double[] getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(double[] coordinates) {
		this.coordinates = coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = Arrays.stream(coordinates.substring(1, coordinates.length()-1).split(","))
			    .map(String::trim).mapToDouble(Double::parseDouble).toArray();
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "GeoPoint [eventId=" + eventId + ", coordinates=" + Arrays.toString(coordinates) + ", address=" + address
				+ "]";
	}
	
	


}
