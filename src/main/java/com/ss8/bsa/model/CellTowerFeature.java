package com.ss8.bsa.model;

import java.util.List;

import com.ss8.bsa.exception.ErrorMessage;

/**
 * POJO to return the response of the Stored Procedure callGeometrySP
 * 
 * @author Srinivas R
 * @version: 1.0 
 */
public class CellTowerFeature {

	private List<String> resultList;

	public CellTowerFeature(List<String> resultList) {
		this.resultList = resultList;
	}

	public List<String> getResultList() {
		return resultList;
	}

	public void setResultList(List<String> resultList) {
		this.resultList = resultList;
	}

	@Override
	public String toString() {
		if (resultList.isEmpty())
			return new ErrorMessage(200, "CellTower Data Not Found").toString();
		else
			return resultList.toString();
	}

}
