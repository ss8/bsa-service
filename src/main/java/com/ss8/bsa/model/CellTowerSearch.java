package com.ss8.bsa.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * POJO to map the request for co-ordinates
 * 
 * @author Srinivas R
 * @version: 1.0 
 */
@Repository
@JsonIgnoreProperties(ignoreUnknown = false)
public class CellTowerSearch implements Serializable {

	private static final long serialVersionUID = 3304674468618815247L;
	
	private String cellid;
	@JsonProperty("valid_from")
	private LocalDateTime validFrom;
	@JsonProperty("valid_to")
	private LocalDateTime validTo;
	private String latlong;
	private String address;

	public CellTowerSearch() {
		super();
	}

	public CellTowerSearch(String cellid, LocalDateTime validFrom, LocalDateTime validTo, String latlong) {
		super();
		this.cellid = cellid;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.latlong = latlong;
	}

	public String getCellid() {
		return cellid;
	}

	public void setCellid(String cellid) {
		this.cellid = cellid;
	}

	public LocalDateTime getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(LocalDateTime validFrom) {
		this.validFrom = validFrom;
	}

	public LocalDateTime getValidTo() {
		return validTo;
	}

	public void setValidTo(LocalDateTime validTo) {
		this.validTo = validTo;
	}

	public String getLatlong() {
		return latlong;
	}

	public void setLatlong(String latlong) {
		this.latlong = latlong;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "CellTowerSearch [cellid=" + cellid + ", validFrom=" + validFrom + ", validTo=" + validTo + ", latlong="
				+ latlong + ", address=" + address + "]";
	}


}
