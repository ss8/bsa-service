package com.ss8.bsa.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.ulisesbocchio.jasyptspringboot.annotation.EncryptablePropertySource;
import com.ulisesbocchio.jasyptspringboot.annotation.EncryptablePropertySources;

/**
 * For loading and mapping the configuration properties from the
 * application.properties file
 */
@Configuration
@ConfigurationProperties(prefix = "bsa")
@EncryptablePropertySources({@EncryptablePropertySource("classpath:application.properties")})
public class ConfigProperties {
	
	public static final String CELL_TYPE_CGI = "CGI";
	public static final String CELL_TYPE_ECGI = "ECGI";
	public static final String CELL_TYPE_SAI = "SAI";
	public static final String CELL_TYPE_TAI = "TAI";
	public static final String CELL_TYPE_RAI = "RAI";
	public static final String CELL_TYPE_LAI = "LAI";
	
	public static final String REST_API_GET_FEATURE = "/getfeature";
	public static final String REST_API_GET_LATLONG_FEATURE = "/getlatlongfeature";
	
	private Map<String, String> mysqlprops = new HashMap<String, String>();

	private String csvfilesSourceFolder;
	private String csvfilesBackupFolder;
	private int csvtojsonArraysize;
	private int dbBatchsize;
	private int csvfolderScanIntervelSeconds;
	private String geojsonRestUrl;
	private int produceroneDelayInMs;
	private int producertwoDelayInMs;
	private int consumerlastDelayInMs;
	private String supportedCellTypes;
	
	public Map<String, String> getMysqlprops() {
		return mysqlprops;
	}

	public void setMysqlprops(Map<String, String> mysqlprops) {
		this.mysqlprops = mysqlprops;
	}

	public String getCsvfilesSourceFolder() {
		return csvfilesSourceFolder;
	}

	public void setCsvfilesSourceFolder(String csvfilesSourceFolder) {
		this.csvfilesSourceFolder = csvfilesSourceFolder;
	}

	public String getCsvfilesBackupFolder() {
		return csvfilesBackupFolder;
	}

	public void setCsvfilesBackupFolder(String csvfilesBackupFolder) {
		this.csvfilesBackupFolder = csvfilesBackupFolder;
	}

	public int getCsvtojsonArraysize() {
		return csvtojsonArraysize;
	}

	public void setCsvtojsonArraysize(int csvtojsonArraysize) {
		this.csvtojsonArraysize = csvtojsonArraysize;
	}

	public int getDbBatchsize() {
		return dbBatchsize;
	}

	public void setDbBatchsize(int dbBatchsize) {
		this.dbBatchsize = dbBatchsize;
	}

	public int getCsvfolderScanIntervelSeconds() {
		return csvfolderScanIntervelSeconds;
	}

	public void setCsvfolderScanIntervelSeconds(int csvfolderScanIntervelSeconds) {
		this.csvfolderScanIntervelSeconds = csvfolderScanIntervelSeconds;
	}

	public String getGeojsonRestUrl() {
		return geojsonRestUrl;
	}

	public void setGeojsonRestUrl(String geojsonRestUrl) {
		this.geojsonRestUrl = geojsonRestUrl;
	}

	public int getProduceroneDelayInMs() {
		return produceroneDelayInMs;
	}

	public void setProduceroneDelayInMs(int produceroneDelayInMs) {
		this.produceroneDelayInMs = produceroneDelayInMs;
	}

	public int getProducertwoDelayInMs() {
		return producertwoDelayInMs;
	}

	public void setProducertwoDelayInMs(int producertwoDelayInMs) {
		this.producertwoDelayInMs = producertwoDelayInMs;
	}

	public int getConsumerlastDelayInMs() {
		return consumerlastDelayInMs;
	}

	public void setConsumerlastDelayInMs(int consumerlastDelayInMs) {
		this.consumerlastDelayInMs = consumerlastDelayInMs;
	}

	public String getSupportedCellTypes() {
		return supportedCellTypes;
	}

	public void setSupportedCellTypes(String supportedCellTypes) {
		this.supportedCellTypes = supportedCellTypes;
	}
	

}