package com.ss8.bsa.config;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.ulisesbocchio.jasyptspringboot.annotation.EncryptablePropertySource;
import com.ulisesbocchio.jasyptspringboot.annotation.EncryptablePropertySources;

@Configuration
@EncryptablePropertySources({@EncryptablePropertySource("classpath:application.properties")})
public class DatabaseConfig {

	@Autowired
	private ConfigProperties cfg;

	@Bean(name = "datasourceone")
	public DataSource createDataSourceOne() {
		Map<String, String> mysqlProps = cfg.getMysqlprops();
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(mysqlProps.get("datasource_driver_class_name"));
		dataSource.setUrl(mysqlProps.get("datasource_url"));
		dataSource.setUsername(mysqlProps.get("datasource_username"));
		dataSource.setPassword(mysqlProps.get("datasource_password"));
		return dataSource;
	}

	@Bean(name = "jdbcdatasourceone")
	@Autowired
	public JdbcTemplate createJdbcTemplateOne(@Qualifier("datasourceone") DataSource dsOne) {
		return new JdbcTemplate(dsOne);
	}

}
