package com.ss8.bsa.exception;

/**
 * Used for generating an error message with Status message and Code
 * 
 * @author Srinivas R
 * @version: 1.0 
 */
public class ErrorMessage {
	
	private int statusCode;
	private String statusMessage;

		
	public ErrorMessage() {
	}


	public ErrorMessage(int statusCode, String statusMessage) {
		this.statusMessage = statusMessage;
		this.statusCode = statusCode;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}


	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}


	@Override
	public String toString() {
		return "{\n" + "\"Code\" : " +statusCode + "," + "\n\"Message\" : " +"\"" +statusMessage +"\"" +"\n}";
	}
	
	

}
