#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import sys
import csv
import time

class LonLat:
	'''LonLat is a class to hold a longitude, latitude pair
	'''
	
	lon = 0
	lat = 0
	
	def __init__(self, lon, lat):
		'''The LonLat constructor
		
		Args:
			lon (float): Longitude in degrees. East is positive
			lat (float): Latitude in degrees. North is positive
		'''
		
		self.lon = lon
		self.lat = lat
	
	def offset(self, azimuth, distance):
		'''The offset method returns a LonLat that is offset from self
		
		Note:
			This method assumes a spherical Earth
		
		Args:
			azimuth (float): Azimuth in degrees
			distance (float): Distance in kilometres
		
		Returns:
			A LonLat that is offset from self by distance km in the azimuth direction
		'''
		
		azimuth = math.radians(azimuth)
		lat1 = math.radians(self.lat)
		lon1 = math.radians(self.lon)
		R = 6378.1 # radius of the Earth in km. Assumes spherical Earth

		lat2 = math.asin(math.sin(lat1) * math.cos(distance/R) + 
						 math.cos(lat1) * math.sin(distance/R) * math.cos(azimuth))

		lon2 = lon1 + math.atan2(math.sin(azimuth) * math.sin(distance/R) * math.cos(lat1),
								 math.cos(distance/R) - math.sin(lat1) * math.sin(lat2))

		return LonLat(math.degrees(lon2), math.degrees(lat2))
	
	def __str__(self):
		'''__str__ returns a string representation of a LonLat object
		
		Returns:
			A string containing the longitude and latitude separated by a space and presented in degrees
		'''
		
		return '[{}, {}]'.format(self.lon, self.lat)

def create_polygon(lon, lat, azimuth, width, distance):
	'''Generate a polygon describing the cell coverage
	
	Note:
		Assumes that the cell coverage is a segment of a circle centered on the tower
	
	Args:
		lon (float): The longitude in degrees of the tower
		lat (float): The latitude in degrees of the tower
		azimuth (float): The azimuth in degrees of the center line of the cell segment
		width (float): The arc width in degrees of the cell segment
		distance (float): The distance in kilometres of the cell segment
	
	Returns:
		A comma separated list of locations. Each location is a specified as lon<space>lat in degrees
		The last location is the same as the first location to create a closed polygon
	'''
	
	loc = LonLat(lon, lat)
	start_az = azimuth - width/2
	end_az = azimuth + width/2
	arc_incr = 3
	
	loc_list = []
	if width < 360:
		loc_list.append(str(loc)) # Don't include tower in polygon if coverage is full circle
	az = start_az
	while az < end_az:
		loc_list.append((str(loc.offset(az, distance))))
		az += arc_incr
	loc_list.append(str(loc.offset(end_az,distance)))
	if width < 360:
		loc_list.append(str(loc))
	return ','.join(loc_list)
