CREATE DATABASE  IF NOT EXISTS `slpdb_controller` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `slpdb_controller`;
-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: slpdb_controller
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `celltower`
--

DROP TABLE IF EXISTS `celltower`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `celltower` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('CGI','ECGI','SAI','TAI') DEFAULT NULL,
  `cellid` varchar(50) DEFAULT NULL,
  `valid_from` datetime DEFAULT NULL,
  `valid_to` datetime DEFAULT NULL,
  `timezone` varchar(15) DEFAULT NULL,
  `metadata` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `geometry` json DEFAULT NULL,
  `latlong` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `constraint_name` (`cellid`,`type`,`valid_from`,`valid_to`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celltower`
--

LOCK TABLES `celltower` WRITE;
/*!40000 ALTER TABLE `celltower` DISABLE KEYS */;
/*!40000 ALTER TABLE `celltower` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'slpdb_controller'
--
/*!50003 DROP PROCEDURE IF EXISTS `getcelltowerfeature` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getcelltowerfeature`(IN queryparam json)
BEGIN
DECLARE param_items INT UNSIGNED;
DECLARE `_index` INT UNSIGNED DEFAULT 0;
DECLARE strcount int DEFAULT 0;
DECLARE geojsonval text;
DECLARE cellid varchar(200);
DECLARE celltype varchar(250);
DECLARE eventdate varchar(100);
DECLARE eventid varchar(250);
DECLARE `geometry` json;
DECLARE id INT;
DECLARE insertval text;

    IF queryparam IS NOT NULL THEN
		set @params =queryparam;

		set param_items =  JSON_LENGTH(@params);


		DROP TEMPORARY TABLE IF EXISTS tempparsetbl;
		CREATE TEMPORARY TABLE IF NOT EXISTS tempparsetbl
		(id int,cellid varchar(50),celltype varchar(500),eventdate TIMESTAMP,eventid int,`geometry` json DEFAULT NULL);

		WHILE `_index` < param_items DO

			set id=`_index`+1;
			set cellid = JSON_EXTRACT(@params,CONCAT('$[', `_index`, '].cell_id'));
			set eventid=JSON_EXTRACT(@params,CONCAT('$[', `_index`, '].event_id'));
			If (eventid is NULL || eventid ='""') THEN
			set geojsonval= CONCAT("'","{",'"cell_id":',cellid,",",'"event_id":',eventid,",",'"errormsg":',"""event id invalid""","}","'");
			set eventid=0;
			ELSE
				set geojsonval= CONCAT("'","{",'"cell_id":',cellid,",",'"event_id":',eventid,",",'"errormsg":',"""cell tower data not found""","}","'");
			END IF;

			set strcount =LENGTH(cellid) - LENGTH(REPLACE(cellid, '-', ''));
			set celltype=JSON_EXTRACT(@params,CONCAT('$[', `_index`, '].cell_type'));

			if (FIND_IN_SET(JSON_UNQUOTE(celltype),"SAI,TAI,CGI,ECGI")=0)   THEN
				set geojsonval= CONCAT("'","{",'"cell_id":',cellid,",",'"event_id":',eventid,",",'"errormsg":',"""cell type is invalid""","}","'");
			END IF;
			set eventdate=JSON_EXTRACT(@params,CONCAT('$[', `_index`, '].eventdate'));
			if (eventdate is NULL OR eventdate = '""') THEN
				set geojsonval= CONCAT("'","{",'"cell_id":',cellid,",",'"event_id":',eventid,",",'"errormsg":',"""event date is invalid""","}","'");
				set eventdate='"2009-01-01 00:00:00"';
			ELSE
				select STR_TO_DATE(JSON_UNQUOTE(eventdate),'%Y-%m-%d %T') into @eventdt;
				IF @eventdt IS NULL THEN
					set geojsonval= CONCAT("'","{",'"cell_id":',cellid,",",'"event_id":',eventid,",",'"errormsg":',"""event date is invalid""","}","'");
					set eventdate='"2009-01-01 00:00:00"';
				END IF;
			END IF;
			if (JSON_UNQUOTE(celltype)="ECGI" OR JSON_UNQUOTE(celltype)="TAI" OR JSON_UNQUOTE(celltype)="SAI") THEN
				IF((select JSON_UNQUOTE(cellid) REGEXP '^[0-9]{1,10}[-][0-9]{1,10}[-][0-9]{1,10}$')=0) THEN
				   set geojsonval= CONCAT("'","{",'"cell_id":',cellid,",",'"event_id":',eventid,",",'"errormsg":',"""Invalid cellid""","}","'");
				END IF;
			END IF;
			IF (JSON_UNQUOTE(celltype)='CGI') THEN
				IF((select JSON_UNQUOTE(cellid) REGEXP '^[0-9]{1,10}[-][0-9]{1,10}[-][0-9]{1,10}[-][0-9]{1,10}$')=0) THEN
					set geojsonval= CONCAT("'","{",'"cell_id":',cellid,",",'"event_id":',eventid,",",'"errormsg":',"""Invalid cellid""","}","'");
				END IF;
			END IF;

			set insertval=CONCAT(id,',',cellid,',',celltype,',',eventdate,',',eventid,',',geojsonval);

			set @str =CONCAT('Insert into tempparsetbl values (',insertval,')');
			IF (id IS NOT NULL AND cellid IS NOT NULL AND celltype IS NOT NULL AND eventdate IS NOT NULL AND eventid IS NOT NULL AND geojsonval IS NOT NULL) THEN
				PREPARE stmt FROM @str;
				EXECUTE stmt;
				DEALLOCATE PREPARE stmt;
			ELSE
			   SIGNAL SQLSTATE '45000'
			   SET MESSAGE_TEXT = 'Parameter Doesnot exist';
			END IF;
			SET `_index` := `_index` + 1;
		END WHILE;



		DROP TEMPORARY TABLE IF EXISTS tempcelltowertbl;
		CREATE TEMPORARY TABLE IF NOT EXISTS tempcelltowertbl
		(cellid varchar(50),cellgeometry JSON,celleventid int);

		insert into tempcelltowertbl(cellid,cellgeometry,celleventid)
		select tempparsetbl.cellid,IFNULL(celltower.geometry,tempparsetbl.geometry),tempparsetbl.eventid from celltower RIGHT join tempparsetbl  on celltower.cellid=tempparsetbl.cellid and celltower.type=tempparsetbl.celltype and tempparsetbl.eventdate>=celltower.valid_from and tempparsetbl.eventdate<=celltower.valid_to;
		Update  tempcelltowertbl set cellgeometry=JSON_INSERT(cellgeometry,'$.features[0].properties.event_id',celleventid) where cellid in (select cellid from celltower);
		select cellgeometry as `geometry` from tempcelltowertbl;



	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-11 15:56:23
