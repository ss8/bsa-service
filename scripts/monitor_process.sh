#!/bin/bash

# ---------------------------------------------------------------------------------
# Script Name : monitor_process.sh
# ---------------------------------------------------------------------------------

result=`ps aux | grep -i "geometry_rest_app.py" | grep -v "grep" | wc -l`
if [ $result -ge 1 ]
   then
        echo "Geometry process is running"
   else
	echo "Starting Geometry process"
        sh start_geometry_process.sh
fi

check=`ps aux | grep -i "slp-bsalmanac" | grep -v "grep" | wc -l`
if [ $check -ge 1 ]
then
        echo "BSA process is running"
else
	echo "Starting BSA process"
	sh start_slp_process.sh
fi

