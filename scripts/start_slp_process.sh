#!/bin/bash

# ---------------------------------------------------------------------------------
# Script Name : start_slp_process.sh
# ---------------------------------------------------------------------------------

source "$(pwd)/config_bsa.cfg"
key=ss8
JVM_OPTIONS=" -Xms256M -Xmx2G -XX:NewSize=256M -XX:+UseConcMarkSweepGC -XX:CMSInitiatingOccupancyFraction=70 "

result=`ps aux | grep -i "slp-bsalmanac" | grep -v "grep" | wc -l`
if [ $result -ge 1 ]
   then
	echo "slp-bsalmanac :: Process is already running"       
   else
        echo "Starting process :: slp-bsalmanac"
	cd $BSA_LOG_DIR
	rm -rf slp-bsalmanac.out
	cd $BSA_ROOT_DIR
  	#java -jar $JVM_OPTIONS *.jar --jasypt.encryptor.password=$key>$BSA_LOG_DIR/slp-bsalmanac.out 2>&1 &
  	java -jar $JVM_OPTIONS *.jar >$BSA_LOG_DIR/slp-bsalmanac.out 2>&1 &
fi


