#!/bin/bash

# ---------------------------------------------------------------------------------
# Script Name : stop_geometry_process.sh
# ---------------------------------------------------------------------------------

process_id1=`ps -ef | grep "geometry_rest_app.py" | grep -v grep | awk '{print $2}'`

if [ $process_id1 > 0 ]
then
   echo "Stopping the process : geometry_rest_app , with process id : $process_id1"
   process_killed=`ps -ef | grep "geometry_rest_app.py" | grep -v grep | awk '{print $2}' |xargs kill`
else
   echo "geometry_rest_app :: No such process to Kill: The Process may not be running"
fi

