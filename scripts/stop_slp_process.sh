#!/bin/bash

# ---------------------------------------------------------------------------------
# Script Name : stop_slp_process.sh
# ---------------------------------------------------------------------------------

process_id1=`ps -ef | grep "slp-bsalmanac" | grep -v grep | awk '{print $2}'`

if [ $process_id1 > 0 ]
then
   echo "Stopping the process : slp-bsalmanac, with process id : $process_id1"
   process_killed=`ps -ef | grep "slp-bsalmanac" | grep -v grep | awk '{print $2}' |xargs kill`
else
   echo "slp-bsalmanac :: No such process to Kill: The Process may not be running"
fi

