###########################################################################
## BSA Rest Interface for GeoJson Conversion							 ##
###########################################################################

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from flask import Flask, request, Response
import ast
from collections import OrderedDict
import os
import logging
import polygonCordinate

from logging.handlers import RotatingFileHandler


APP = Flask(__name__)

log_file_path = "/opt/slp-logs/geometry_rest_service.log"

logger = logging.getLogger("Rotating Log")
logger.setLevel(logging.INFO)
# add a rotating handler
handler = RotatingFileHandler(log_file_path, maxBytes=1024*1024, backupCount=5)
logger.addHandler(handler)


@APP.route("/api/geo", methods=["POST"])
def process_geometry():

    global feature_id
    json_data = json.loads(request.data)
    
    json_array = []
 
    try:
        if os.path.isfile('featureid.txt'):
            with open('featureid.txt', 'r+') as idfile:
                # Non empty file exists
                feature_id = idfile.readline()
        
                if(feature_id == ''):
                    feature_id = '1'
                    idfile.seek(0)
                    idfile.truncate()
                    idfile.close()
                else:
                    feature_id = '1'
                    
                with open('featureid.txt', 'a+') as idfile:
                    print(feature_id)
                    idfile.write(str(int(feature_id) + 1))
                    idfile.close()
    
    except OSError as e:
        logger.error("featureid.txt not found")
        #print("featureid.txt not found")
        
    
    try:
        
        for jobject in json_data:
            
            #feature_id = '1'
            Type = jobject["Type"]
            ID = jobject["ID"]
            lon = float(jobject["lon"])
            lat = float(jobject["lat"])
            azimuth = float(jobject["azimuth"])
            width = float(jobject["width"])
            range = float(jobject["range"])
            valid_from = jobject["valid_from"]
            valid_to = jobject["valid_to"]
            metadata = jobject["metadata"]
            address = jobject["address"]
    
            if(lon==0 or lat==0 or azimuth==0 or width==0 or range==0):
                 geometries = ""
            else:
                polygon = (polygonCordinate.create_polygon(lon, lat, azimuth, width, range))
                geos_point = OrderedDict()
                geos_point['type'] = 'Feature'
                geos_point['id'] = 'cell-coverage.' + ID
                geos_point['geometry'] = OrderedDict([('type', 'Point'), ('coordinates', [(lon), (lat)])])
                geos_point['properties'] = OrderedDict([('id', feature_id), ('cellid', ID), ('valid_from', valid_from), ('valid_to', valid_to), ('srs', 'EPSG:4326'), ('metadata', metadata), ('type', Type), ('address', address)])
        
                geos_poly = OrderedDict()
                geos_poly['type'] = 'Feature'
                geos_poly['id'] = 'cell-coverage.' + str(feature_id)
                geos_poly['geometry'] = OrderedDict([('type', 'Polygon'), ('coordinates', [ast.literal_eval(polygon)])])
                geos_poly['geometry_name'] = 'poly_geom'
                geos_poly['properties'] = OrderedDict([('id', ID)])
                param = {'type': 'EPSG', 'properties': {'code': '4326'}}
                geos = []
                geos.append(geos_point)
                geos.append(geos_poly)
                param = {'type': 'EPSG', 'properties': {'code': '4326'}}
                geometries = OrderedDict([('type', 'FeatureCollection'), ('features', geos), ('crs', param)])
        
            each_row_json = {'type': Type, 'cellid': ID, 'lon': lon, 'lat': lat, 'valid_from': valid_from, 'valid_to' : valid_to, 'metadata' : metadata, 'address' : address, 'geometry' : geometries }
            json_array.append(each_row_json)
    
    except Exception as e1:
        logger.error("geometry_rest_app :: Exception :: "+e1)
        
    finally:   
        return Response(json.dumps(json_array), mimetype="application/json")
    
    
if __name__ == "__main__":

    APP.run(debug=False)





