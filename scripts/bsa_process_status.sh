#!/bin/bash

# ---------------------------------------------------------------------------------
# Script Name : bsa_process_status.sh
# ---------------------------------------------------------------------------------

result_geometry=`ps aux | grep -i "geometry_rest_app.py" | grep -v "grep" | wc -l`
result_slp=`ps aux | grep -i "slp-bsalmanac" | grep -v "grep" | wc -l`

echo ""
if [ $result_slp -ge 1 ] && [ $result_geometry -ge 1 ]
   then
        echo "BSA Service - Status :: OK and Running"
   else
        echo "BSA Service - Status :: Not Running"
        echo "Start the BSA Service using the command :: ./bsa_service --start"
fi
echo ""

