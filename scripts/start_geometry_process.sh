#!/bin/bash

# ---------------------------------------------------------------------------------
# Script Name : start_geometry_process.sh
# ---------------------------------------------------------------------------------

result=`ps aux | grep -i "geometry_rest_app.py" | grep -v "grep" | wc -l`
if [ $result -ge 1 ]
   then
	echo "geometry_rest_app.py :: Process is already running"       
   else
        echo "Starting process :: geometry_rest_app.py"
        nohup python geometry_rest_app.py >/dev/null 2>&1 &
fi


