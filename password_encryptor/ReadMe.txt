
1.	Go to the location of the jasypt-1.9.2.jar
	
	In the Command Line
    Generate an encrypted password as follows:
    
    java -cp jasypt-1.9.2.jar org.jasypt.intf.cli.JasyptPBEStringEncryptionCLI input="<password>" password=<secret-key> algorithm=PBEWithMD5AndDES

    where 
	<password> = password to be encrypted
	<secret-key> = secret master key to be provided for the encryption

	For Example:
	
	D:\monitoring-system\password_encryptor>java -cp jasypt-1.9.2.jar org.jasypt.intf.cli.JasyptPBEStringEncryptionCLI input=ss81234 password=ss8 algorithm=PBEWithMD5AndDES

	The following output is generated: 

	------------------------------------------------------------------------------------------------------------------------
	
	----ENVIRONMENT-----------------

	Runtime: Oracle Corporation Java HotSpot(TM) 64-Bit Server VM 25.192-b12


	----ARGUMENTS-------------------

	algorithm: PBEWithMD5AndDES
	input: ss81234
	password: ss8


	----OUTPUT----------------------
	
	M132pmorQXrHhWll2Gi/mQ==
		
	------------------------------------------------------------------------------------------------------------------------


2.	Copy the generated encrypted key msHd0hPJRMjD7ZIdRQYoqA== and use it in the application.properties file as follows:

	mysql_password=ENC(pHQ9DKf9BVDVdDvK6F6gTw==)
	

3.  Use this option when running the jar:
	--jasypt.encryptor.password=ss8
	
	
	
