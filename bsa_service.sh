#!/bin/bash

# ---------------------------------------------------------------------------------
# Script Name : bsa_service.sh
# ---------------------------------------------------------------------------------

cd scripts
source config_bsa.cfg

case "$1" in
  --deploy)
	if [ `id -u` -ne 0 ]; then
    	echo "You need to be root to execute this script. Exiting.."
    	exit 1
	fi
        echo "Starting BSA Service Deployment"
	mkdir -p $BSA_LOG_DIR
	chown -R support:support  $BSA_LOG_DIR
	chmod -R 755 $BSA_LOG_DIR
	(crontab -l ; echo "0,30 * * * * $BSA_SCRIPTS_DIR/monitor_process.sh") | sort - | uniq - | crontab -
	echo "Monitoring Script :: Started.."
        sh start_slp_process.sh
        sh start_geometry_process.sh
	echo "BSA Service Deployed.."
    ;;
  --start)
    echo "Starting BSA Service.."
	sh start_geometry_process.sh
	sh start_slp_process.sh
    ;;
  --stop)
	echo "Stopping BSA Service.."
    	sh stop_slp_process.sh
	sh stop_geometry_process.sh	
    ;;
 --status)
        sh bsa_process_status.sh
    ;;
  *)
    echo "Usage: $0 --{deploy|start|stop|status}" >&2
    exit 1
    ;;
esac

