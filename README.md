##########################################################################
Application Name: slp-bsalmanac
Version: R.1.1.0
##########################################################################

Features and ChangeLog:
1. BS Almanac Processor with CSV, JSON, and Cache Features, is custom version for integration with the Fusion project.
2. This is the custom base version.
3. Added password encryption with jasypt. For encryption instructions, refer the ReadMe in password_encryptor folder.

